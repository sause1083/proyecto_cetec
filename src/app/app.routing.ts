import { HorariosComponent } from './pages/componentes-alumno/horarios/horarios.component';
import { BoletasComponent } from './pages/componentes-alumno/boletas/boletas.component';
import { CalificacionesParcialesComponent } from './pages/componentes-alumno/calificaciones-parciales/calificaciones-parciales.component';
import { InicioAdministracionComponent } from './pages/inicio-administracion/inicio-administracion.component';
import { InicioDocenteComponent } from './pages/inicio-docente/inicio-docente.component';
import { InicioAlumnoComponent } from './pages/inicio-alumno/inicio-alumno.component';
import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';


const routes: Routes =[
    { path: 'SIC',component: HomeComponent },
    { path: 'inicio-alumno', component: InicioAlumnoComponent },
    { path: 'inicio-docente', component:InicioDocenteComponent },
    { path:'calificaciones-parciales', component: CalificacionesParcialesComponent },
    { path: 'boleta', component: BoletasComponent},
    { path: 'horarios', component: HorariosComponent},

    { path: 'inicio-admin', component:InicioAdministracionComponent },
    { path: '**', redirectTo: 'SIC', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
      useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
