import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';

import { AppComponent } from './app.component';


import { HomeModule } from './home/home.module';
import { InicioAlumnoComponent } from './pages/inicio-alumno/inicio-alumno.component';
import { InicioDocenteComponent } from './pages/inicio-docente/inicio-docente.component';
import { InicioAdministracionComponent } from './pages/inicio-administracion/inicio-administracion.component';
import { CalificacionesParcialesComponent } from './pages/componentes-alumno/calificaciones-parciales/calificaciones-parciales.component';
import { BoletasComponent } from './pages/componentes-alumno/boletas/boletas.component';
import { HorariosComponent } from './pages/componentes-alumno/horarios/horarios.component';


@NgModule({
  declarations: [
    AppComponent,
    InicioAlumnoComponent,
    InicioDocenteComponent,
    InicioAdministracionComponent,
    CalificacionesParcialesComponent,
    BoletasComponent,
    HorariosComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
    HomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
